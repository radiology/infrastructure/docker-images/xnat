FROM tomcat:9.0.54-jdk8-openjdk-slim


ENV XNAT_VER 1.8.2

#Install debugging tools
RUN apt-get update && apt-get install -y postgresql-client\
                                         gettext-base\
                                         curl\
                                         unzip

# Download XNAT
#FIXME: The find/chmod commands are there because the permissions in the xnat war are not correct in version 1.8.2
# This should be fixed upstream at some point.
RUN curl -k -L -o xnat-web-${XNAT_VER}.war  https://api.bitbucket.org/2.0/repositories/xnatdev/xnat-web/downloads/xnat-web-${XNAT_VER}.war &&\
    unzip -o xnat-web-${XNAT_VER}.war -d  $CATALINA_HOME/webapps/ROOT && \
    find /usr/local/tomcat/webapps/ROOT/ -type d -exec chmod 755 -- {} + &&\
    find /usr/local/tomcat/webapps/ROOT/ -type f -exec chmod 644 -- {} + && \
    rm xnat-web-${XNAT_VER}.war

# Add context.xml for tomcat 9 compatibility
COPY config/context.xml $CATALINA_HOME/webapps/ROOT/META-INF/

# Set run scripts
COPY config/xnat-conf.template /xnat-conf.template
COPY scripts/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

# Set docker stuff
EXPOSE 8080
EXPOSE 8104
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=10 CMD wget --quiet --tries=1 -O - http://localhost:8080/data/JSESSION > /dev/null 2>&1
ENTRYPOINT ["bash"]
CMD ["/docker-entrypoint.sh"]