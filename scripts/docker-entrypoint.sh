#!/bin/bash

# Setting default values
if [[ -z $XNAT_HOME &&  -z $XNAT_DATA ]] ; then 
    export XNAT_DATA=/data/xnat
    export XNAT_HOME=${XNAT_DATA}/home
elif [ -z  $XNAT_HOME ] ; then 
    export XNAT_HOME=${XNAT_DATA}/home
elif [ -z $XNAT_DATA ] ; then
    export XNAT_DATA=${XNAT_HOME}/..
fi

# Create dat folders
mkdir -p ${XNAT_HOME}/config
mkdir -p ${XNAT_HOME}/logs
mkdir -p ${XNAT_HOME}/plugins
mkdir -p ${XNAT_HOME}/work
mkdir -p ${XNAT_DATA}/archive
mkdir -p ${XNAT_DATA}/prearchive
mkdir -p ${XNAT_DATA}/pipeline
mkdir -p ${XNAT_DATA}/build
mkdir -p ${XNAT_DATA}/cache
mkdir -p ${XNAT_DATA}/temp

# Setting XNAT config
export CATALINA_OPTS="$CATALINA_OPTS -Dxnat.home=${XNAT_HOME}"
if [ ! -e   ${XNAT_HOME}/config/xnat-conf.properties ] ; then 
    envsubst < /xnat-conf.template > ${XNAT_HOME}/config/xnat-conf.properties
fi

# Starting tomcat
echo "CATALINA_OPTS: ${CATALINA_OPTS}"
catalina.sh run
