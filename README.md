# xnat

Docker images & Helm charts for XNAT

# Docker Image
The different XNAT release are organies as branches: release/{xnat-version}. Master branche should be the latest version.

# Helm Charts

Available charts in this repo:
  * XNAT
### Available settings

  * deployment:
      * name: xnat
      * host: local-dev-xnat.infra-dev.k3d
      * account: rsvc-infra-dev
  * config:
    * global:
      * db_name: xnat
      * db_user: xnat
    * xnat:
      * data: /xnatdata/
      * home: /xnatdata/home
  * secrets:
    * global:
      * db_password: xnat-password

### Usage information and such
  Cehck the values file in ./xnat/values.
  Default admin password once the appliction is up:
  user:admin
  passwd:admin

